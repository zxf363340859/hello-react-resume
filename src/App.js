import React, { Component } from 'react';
import './App.less';
import BasicInfo from './components/BasicInfo';
import Education from './components/Education';
import PersonInfo from './components/PersonInfo';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      basicInfo: {}
    };
    fetch('http://localhost:3000/person')
      .then(response => response.json())
      .then(result => {
        // const { name, age, description, educations } = result;
        this.setState({
          basicInfo: result
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const info = this.state.basicInfo;
    const { educations } = info;
    return (
      <main className="app">
        <BasicInfo info={info} />
        <PersonInfo info={info} />
        <Education edu={educations} />
      </main>
    );
  }
}

export default App;
