import React from 'react';

const PersonInfo = props => {
  return (
    <div>
      <section className="extend-info">
        <h3>ABOUT ME</h3>
        <p id="info-desc">{props.info.description}</p>
      </section>
    </div>
  );
};

export default PersonInfo;
