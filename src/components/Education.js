import React from 'react';

const Education = props => {
  return (
    <div>
      <section className="extend-info">
        <h3>EDUCATION</h3>
        <ul className="edu" id="edu">
          {props.edu !== undefined
            ? props.edu.map((edu, index) => (
                <li key={index}>
                  <div>
                    <span className="edu-year">
                      <strong>{edu.year}</strong>
                    </span>
                    <div className="edu-desc">
                      <strong>{edu.title}</strong> <br />
                      {edu.description}
                    </div>
                  </div>
                </li>
              ))
            : null}
        </ul>
      </section>
    </div>
  );
};

export default Education;
