import React from 'react';
import IMG from '../assets/avatar.jpg';
const BasicInfo = props => {
  return (
    <div>
      <section className="basic-info">
        <ul>
          <li className="img-li">
            <img src={IMG} alt="" />
          </li>
          <li className="hello">
            <p>HELLO,</p>
          </li>
          <li className="desc">
            <p>
              {/* eslint-disable-next-line react/prop-types */}
              MY NAME IS <span id="info-name">{props.info.name}</span>
              {/* eslint-disable-next-line react/prop-types */}
              <span id="info-age"> {props.info.age}</span> AND THIS IS MY
              RESUME/CV MY RESUME/CV MY RESUME/CV MY RESUME/CV
            </p>
          </li>
        </ul>
      </section>
    </div>
  );
};

export default BasicInfo;
